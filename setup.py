from setuptools import setup, find_packages

setup(
    name="automation-shared-resources",
    version='0.0.6',
    scripts=[] ,
    author="Elmer Brillantes",
    author_email="elmer.brillantes@dtn.com",
    packages=find_packages(),
    package_data={'': ['*.robot', '*.py', '*.json', '*.txt']},
    include_package_data=True,
    long_description="Test Automation Shared Resources"
 )
