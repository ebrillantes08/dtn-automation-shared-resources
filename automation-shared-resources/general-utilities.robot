*** Settings ***
Library           Collections
Library           DateTime
Library           String

*** Keywords ***
Check If Parameter Is A Number
    [Arguments]    ${parameter}
    ${is_number}    Run Keyword And Return Status    Should Match RegExp    ${parameter}    ^[0-9]+$
    [Return]    ${is_number}

Convert Binary File To Base64
    [Arguments]    ${file_path}
    ${bytes}    Get Binary File    ${file_path}
    ${base64}    Convert Bytes To Base64    ${bytes}
    [Return]    ${base64}

Convert Bytes To Base64
    [Arguments]    ${bytes}
    ${base64}    Evaluate    base64.b64encode($bytes)    base64
    ${base64}    Convert To String    ${base64}
    [Return]    ${base64}

Convert Datetime To Epoch
    [Arguments]    ${datetime}
    ${epoch_datetime}    Convert Date    ${datetime}    epoch
    ${epoch_datetime}    Convert To String    ${epoch_datetime}
    ${epoch_datetime}    Replace String    ${epoch_datetime}    .0    000
    [Return]    ${epoch_datetime}

Convert Numerical Month To Text Format (MMM)
    [Arguments]    ${month}
    ${month}    Convert To Integer    ${month}
    ${month}    Evaluate    calendar.month_abbr[${month}]    calendar
    [Return]    ${month.upper()}

Create List From 1 To ${x}
    @{total_count_list}    Create List
    : FOR    ${current_count}    IN RANGE    1    ${x+1}
    \    Append To List    ${total_count_list}    ${current_count}
    [Return]    @{total_count_list}

Create Numbers List
    [Arguments]    ${start_num}    ${end_num}
    @{list}    Evaluate    list(range(${start_num},(${end_num}+1)))
    [Return]    @{list}

Generate UUID
    ${uuid}    Evaluate    uuid.uuid4()    uuid
    ${uuid}    Convert To String    ${uuid}
    [Return]    ${uuid}

Get Day Of Week From Date With Format %Y-%m-%d
    [Arguments]    ${date}
    ${day}    Convert Date    date=${date}    result_format=%A    date_format=%Y-%m-%d
    [Return]    ${day}

Get Month And Day From Datetime With Format %Y-%m-%dT%H:%M:%S
    [Arguments]    ${datetime}
    ${month}    Get Substring    ${datetime}    5    7
    ${month}    Convert Numerical Month To Text Format (MMM)    ${${month}}
    ${day}    Get Substring    ${datetime}    8    10
    [Return]    ${month}    ${day}

Get Random Value From A List
    [Arguments]    @{list}
    ${value}    Evaluate    str(random.choice(${list}))    random
    [Return]    ${value}

Get Random Value From List
    [Arguments]    ${list}
    [Documentation]    Please use sample format: 'A','B','C','D'
    ${value}    Evaluate    random.choice([${list}])    random
    [Return]    ${value}

Get Travel Date ${x} Days From Now
    ${month}    ${day}    Get Time    month,day    UTC + ${x} day
    ${month}    Convert Numerical Month To Text Format (MMM)    ${month}
    ${travel_date}    Set Variable    ${day}${month}
    [Return]    ${travel_date}

Increment Datetime
    [Arguments]    ${datetime}    ${increment}=+1day    ${format}=%Y-%m-%dT%H:%M:%S
    ${datetime}    Add Time To Date    date=${datetime}    time=${increment}    result_format=${format}    date_format=${format}
    [Return]    ${datetime}

Verify That Actual List Matches Expected List
    [Arguments]    ${actual_list}    ${expected_list}
    Log List    ${actual_list}
    Log List    ${expected_list}
    Run Keyword And Continue On Failure    Lists Should Be Equal    ${actual_list}    ${expected_list}

Verify That Actual Value Does Not Match Expected Value
    [Arguments]    ${actual_value}    ${expected_value}
    Log    Actual Value: ${actual_value}
    Log    Expected Value: ${expected_value}
    ${actual_value}    Convert To String    ${actual_value}
    ${expected_value}    Convert To String    ${expected_value}
    Run Keyword And Continue On Failure    Should Not Be Equal    ${actual_value}    ${expected_value}    Actual value "${actual_value}" matches expected value "${expected_value}"    FALSE

Verify That Actual Value Matches Expected Value
    [Arguments]    ${actual_value}    ${expected_value}    ${custom_error}=Actual value "${actual_value}" does not match expected value "${expected_value}"
    Log    Actual Value: ${actual_value}
    Log    Expected Value: ${expected_value}
    ${actual_value}    Convert To String    ${actual_value}
    ${expected_value}    Convert To String    ${expected_value}
    Run Keyword And Continue On Failure    Should Be Equal    ${actual_value}    ${expected_value}    ${custom_error}    FALSE

Verify That List Contains Value
    [Arguments]    ${list}    ${value}
    Log List    ${list}
    Log    Expected Value: ${value}
    Run Keyword And Continue On Failure    List Should Contain Value    ${list}    ${value}    List "${list}" does not contain value "${value}"

Verify That String Contains Substring
    [Arguments]    ${string}    ${substring}
    Log    Actual String: ${string}
    Log    Expected Substring: ${substring}
    Run Keyword And Continue On Failure    Should Contain    ${string}    ${substring}    String "${string}" does not contain substring "${substring}"    FALSE

Verify That String Contains Substring X Times
    [Arguments]    ${string}    ${substring}    ${occurence}
    Log    Actual String: ${string}
    Log    Expected Substring: ${substring} (displayed ${occurence} time/s only)
    Run Keyword And Continue On Failure    Should Contain X Times    ${string}    ${substring}    ${occurence}    Substring "${substring}" is not found ${occurence} time/s

Verify That String Does Not Contain Substring
    [Arguments]    ${string}    ${substring}
    Log    Actual String: ${string}
    Log    Expected Substring: ${substring}
    Run Keyword And Continue On Failure    Should Not Contain    ${string}    ${substring}    String "${string}" contains substring "${substring}"    FALSE

Verify That String Does Not Match Pattern
    [Arguments]    ${string}    ${pattern}
    Log    Actual String: ${string}
    Log    Expected Pattern: ${pattern}
    Run Keyword And Continue On Failure    Should Not Match RegExp    ${string}    ${pattern}    String "${string}" matches pattern "${pattern}"    FALSE

Verify That String Matches Pattern
    [Arguments]    ${string}    ${pattern}
    Log    Actual String: ${string}
    Log    Expected Pattern: ${pattern}
    Run Keyword And Continue On Failure    Should Match RegExp    ${string}    ${pattern}    String "${string}" does not match pattern "${pattern}"    FALSE

Wait For ${x} Seconds
    Sleep    ${x}
