*** Settings ***
Library           JSONLibrary
Library           RequestsLibrary
Resource          general-utilities.robot

*** Keywords ***
Error Response Is Returned
    [Arguments]    ${error_code}=${empty}    ${error_message}=${empty}    ${error}=${empty}    ${message}=${empty}    ${delimiter}=${empty}
    [Documentation]    Please use 'Delimiter' if multiple error messages are returned and the ordering is not consistent.
    # handling if multiple error messages are returned and the ordering is not consistent
    Run Keyword If    "${delimiter}" != "${empty}" and $error_message != "${empty}"    Sort Response Error Message    ${delimiter}    json_path=$.errorMessage
    Run Keyword If    "${delimiter}" != "${empty}" and $message != "${empty}"    Sort Response Error Message    ${delimiter}    json_path=$.message
    # proceed with actual verification
    Run Keyword If    $error_code != "${empty}"    Verify That Json Value Is Correct    json_path=$.errorCode    expected_json_value="${error_code}"
    Run Keyword If    $error_message != "${empty}"    Verify That Json Value Is Correct    json_path=$.errorMessage    expected_json_value="${error_message.replace('"','\\"')}"
    Run Keyword If    $error != "${empty}"    Verify That Json Value Is Correct    json_path=$.error    expected_json_value="${error}"
    Run Keyword If    $message != "${empty}"    Verify That Json Value Is Correct    json_path=$.message    expected_json_value="${message.replace('"','\\"')}"

Generate Body
    [Arguments]    @{parameters}
    [Documentation]    Format: <field name>,<field value>,<data type> | Example: userName,${username},str
    ${body}    Set Variable    ${empty}
    : FOR    ${parameter}    IN    @{parameters}
    \    ${field_name}    Fetch From Left    ${parameter}    ,
    \    ${data_type}    Fetch From Right    ${parameter}    ,
    \    ${field_value}    Remove String Using RegExp    ${parameter}    ((${field_name},)|(,${data_type}))
    \    ${new_field}    Set Variable If    "${data_type.lower()}" == "str"    "${field_name}":"${field_value}"    "${field_name}":${field_value}
    \    ${body}    Set Variable If    $field_value.lower() != "none" and "${body.replace('"','')}" != "${empty}"    ${body},${new_field}    $field_value.lower() != "none" and "${body.replace('"','')}" == "${empty}"    ${new_field}
    \    ...    ${body}
    \    Set Test Variable    ${body}
    ${body}    Replace String    ${body}    "null"    null
    ${body}    Replace String    ${body}    [null]    null
    ${body}    Set Variable    {${body}}
    [Return]    ${body}

Generate Headers
    [Arguments]    ${transaction_id}=auto    ${authorization_token}=none    ${accept}=application/json    ${accept_language}=none    ${client_id}=none    ${transaction_status_code}=none
    ...    ${security_token}=none    ${sequence_number}=none    ${session_id}=none    ${content_type}=application/json
    ${headers}    Create Dictionary
    ${transaction_id}    Run Keyword If    "${transaction_id}" == "auto"    Generate UUID
    ...    ELSE    Set Variable    ${transaction_id}
    Set Test Variable    ${transaction_id}
    Run Keyword If    "${transaction_id.lower()}" != "none"    Set To Dictionary    ${headers}    transactionId=${transaction_id}
    Run Keyword If    "${authorization_token.lower()}" != "none"    Set To Dictionary    ${headers}    Authorization=${authorization_token}
    Run Keyword If    "${accept.lower()}" != "none"    Set To Dictionary    ${headers}    Accept=${accept}
    Run Keyword If    "${accept_language.lower()}" != "none"    Set To Dictionary    ${headers}    Accept-Language=${accept_language}
    Run Keyword If    "${client_id.lower()}" != "none"    Set To Dictionary    ${headers}    client_id=${client_id}
    Run Keyword If    "${transaction_status_code.lower()}" != "none"    Set To Dictionary    ${headers}    transactionStatusCode=${transaction_status_code}
    Run Keyword If    "${security_token.lower()}" != "none"    Set To Dictionary    ${headers}    securityToken=${session_token}
    Run Keyword If    "${sequence_number.lower()}" != "none"    Set To Dictionary    ${headers}    sequenceNumber=${sequence_number}
    Run Keyword If    "${session_id.lower()}" != "none"    Set To Dictionary    ${headers}    sessionId=${session_id}
    Run Keyword If    "${content_type.lower()}" != "none"    Set To Dictionary    ${headers}    Content-Type=${content_type}
    [Return]    ${headers}

Generate Query Parameters
    [Arguments]    @{parameters}
    [Documentation]    Format: <field name>,<field value> | Example: userName,${username}
    ${query_parameters}    Set Variable    ${empty}
    : FOR    ${parameter}    IN    @{parameters}
    \    ${parameter}    Split String    ${parameter}    ,
    \    ${field_name}    Set Variable    ${parameter[0]}
    \    ${field_value}    Set Variable    ${parameter[1]}
    \    ${new_field}    Set Variable    ${field_name}=${field_value}
    \    ${query_parameters}    Set Variable If    "${field_value.lower()}" != "none" and "${query_parameters}" != "${empty}"    ${query_parameters}&${new_field}    "${field_value.lower()}" != "none" and "${query_parameters}" == "${empty}"    ?${new_field}
    \    ...    ${query_parameters}
    \    Set Test Variable    ${query_parameters}
    [Return]    ${query_parameters}

Get Json Value
    [Arguments]    ${json_object}    ${json_path}
    ${json_object}    To Json    ${json_object}
    ${json_value}    Get Value From Json    ${json_object}    ${json_path}
    ${json_value}    Convert JSON To String    ${json_value}
    ${json_value}    Get Substring    ${json_value}    1    -1
    [Return]    ${json_value}

Response Is Empty
    Verify That Actual Value Matches Expected Value    ${response.text}    ${empty}

Response Status Code Is ${status_code}
    Verify That Actual Value Matches Expected Value    ${response.status_code}    ${status_code}

Send Request
    [Arguments]    ${method}    ${url}    ${headers}=${empty}    ${body}=${empty}    ${query_parameters}=${empty}    ${host}=${${env}_host}
    ...    ${timeout}=60
    Create Session    ins    ${host}
    ${response}    Run Keyword If    "${method.lower()}" != "get"    ${method} Request    ins    uri=${url}${query_parameters}    headers=${headers}
    ...    data=${body}    timeout=${timeout}
    ...    ELSE    ${method} Request    ins    uri=${url}${query_parameters}    headers=${headers}    timeout=${timeout}
    Set Test Variable    ${response}
    Log    ${response.headers}
    Log    ${response.text}
    [Teardown]    Delete All Sessions

Sort Response Error Message
    [Arguments]    ${delimiter}    ${json_path}
    ${current_error_message}    Get Json Value    ${response.text}    ${json_path}
    ${current_error_message}    Remove String Using RegExp    ${current_error_message}    ((^\\")|(\\"$))
    @{current_error_message_list}    Split String    ${current_error_message}    ${delimiter}
    Sort List    ${current_error_message_list}
    ${new_error_message}    Set Variable    ${empty}
    : FOR    ${item}    IN    @{current_error_message_list}
    \    ${new_error_message}    Set Variable If    $new_error_message != "${empty}"    ${new_error_message}${delimiter}${item}    ${item}
    ${str_response_body}    Convert To String    ${response.text}
    ${str_response_body}    Replace String    ${str_response_body}    ${current_error_message}    ${new_error_message}
    Set Test Variable    ${response.text}    ${str_response_body}

Transaction Id Is Auto-Generated
    ${headers}    Convert To String    ${response.headers}
    Verify That String Matches Pattern    ${headers}    \\'((transactionId)|(UUID))\\'\\: \\'[\\w]{8}\\-[\\w]{4}\\-[\\w]{4}\\-[\\w]{4}\\-[\\w]{12}\\'

Transaction Id Is Returned
    ${headers}    Convert To String    ${response.headers}
    Verify That String Matches Pattern    ${headers}    \\'((transactionId)|(UUID))\\'\\: \\'${transaction_id.replace('-','\\-')}\\'

Verify That Json Path Does Not Exist
    [Arguments]    ${json_path}
    Log    Json Path: ${json_path}
    ${status}    Run Keyword And Return Status    Get Json Value    ${response.text}    ${json_path}
    Verify That Actual Value Matches Expected Value    ${status}    False

Verify That Json Path Exists
    [Arguments]    ${json_path}
    Log    Json Path: ${json_path}
    ${status}    Run Keyword And Return Status    Get Json Value    ${response.text}    ${json_path}
    Verify That Actual Value Matches Expected Value    ${status}    True

Verify That Json Value Is Correct
    [Arguments]    ${json_path}    ${expected_json_value}
    Log    Expected: ${expected_json_value}
    ${actual_json_value}    Get Json Value    ${response.text}    ${json_path}
    Verify That Actual Value Matches Expected Value    ${actual_json_value}    ${expected_json_value}

Verify That Json Value Matches Pattern
    [Arguments]    ${json_path}    ${expected_pattern}
    Log    Expected: ${expected_pattern}
    ${actual_json_value}    Get Json Value    ${response.text}    ${json_path}
    Verify That String Matches Pattern    ${actual_json_value}    ${expected_pattern}

Verify That Xml Path Does Not Exist
    [Arguments]    ${xml_path}    ${attribute}=${empty}
    Log    Xml Path: ${xml_path}
    ${xml}    Parse Xml    ${response.text}
    Run Keyword If    "${attribute}" == "${empty}"    Run Keyword And Continue On Failure    Element Should Not Exist    ${xml}    ${xml_path}
    ...    ELSE    Run Keyword And Continue On Failure    Element Should Not Have Attribute    ${xml}    ${attribute}    ${xml_path}

Verify That Xml Value Is Correct
    [Arguments]    ${xml_path}    ${expected_xml_value}    ${attribute}=${empty}
    Log    Expected: ${expected_xml_value}
    ${xml}    Parse Xml    ${response.text}
    Run Keyword If    "${attribute}" == "${empty}"    Run Keyword And Continue On Failure    Element Text Should Be    ${xml}    ${expected_xml_value}    ${xml_path}
    ...    ELSE    Run Keyword And Continue On Failure    Element Attribute Should Be    ${xml}    ${attribute}    ${expected_xml_value}
    ...    ${xml_path}
