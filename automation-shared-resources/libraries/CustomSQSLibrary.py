import boto3

class CustomSQSLibrary(object):

    def add_queue_place_item_to_sqs(self, account_id, queue_name, aws_access_key_id, aws_secret_access_key, aws_session_token, gds, pnr, pcc, queue_number, category, region='us-west-2'):
        sqs = boto3.client('sqs', region, aws_access_key_id=aws_access_key_id, aws_secret_access_key=aws_secret_access_key, aws_session_token=aws_session_token)
        response = sqs.send_message(
            QueueUrl='https://sqs.'+region+'.amazonaws.com/'+account_id+'/'+queue_name,
            DelaySeconds=10,
            MessageBody=('{"recordLocator":"'+pnr+'","category":'+category+',"queueNumber":"'+queue_number+'","pcc":"'+pcc+'","gds":"'+gds+'"}')
        )
        return response
    
    def get_queue_place_item_in_sqs(self, account_id, queue_name, aws_access_key_id, aws_secret_access_key, aws_session_token, region='us-west-2'):
        sqs = boto3.client('sqs', region, aws_access_key_id=aws_access_key_id, aws_secret_access_key=aws_secret_access_key, aws_session_token=aws_session_token)
        response = sqs.receive_message(
            QueueUrl='https://sqs.'+region+'.amazonaws.com/'+account_id+'/'+queue_name,
            AttributeNames=['All'],
            MaxNumberOfMessages=1,
            MessageAttributeNames=['All'],
            WaitTimeSeconds=20
        )
        return response
    
    def delete_queue_place_item_from_sqs(self, account_id, queue_name, aws_access_key_id, aws_secret_access_key, aws_session_token, receipt_handle, region='us-west-2'):
        sqs = boto3.client('sqs', region, aws_access_key_id=aws_access_key_id, aws_secret_access_key=aws_secret_access_key, aws_session_token=aws_session_token)
        response = sqs.delete_message(
            QueueUrl='https://sqs.'+region+'.amazonaws.com/'+account_id+'/'+queue_name,
            ReceiptHandle=receipt_handle
        )
        return response
    
    def get_and_delete_queue_place_item_from_sqs(self, account_id, queue_name, aws_access_key_id, aws_secret_access_key, aws_session_token, region='us-west-2'):
        sqs = boto3.client('sqs', region, aws_access_key_id=aws_access_key_id, aws_secret_access_key=aws_secret_access_key, aws_session_token=aws_session_token)
        get_response = sqs.receive_message(
            QueueUrl='https://sqs.'+region+'.amazonaws.com/'+account_id+'/'+queue_name,
            AttributeNames=['All'],
            MaxNumberOfMessages=1,
            MessageAttributeNames=['All'],
            WaitTimeSeconds=20
        )
        message = get_response['Messages'][0]
        receipt_handle = message['ReceiptHandle']
        delete_response = sqs.delete_message(
            QueueUrl='https://sqs.'+region+'.amazonaws.com/'+account_id+'/'+queue_name,
            ReceiptHandle=receipt_handle
        )
        return get_response, delete_response